import {Component, Input, OnInit} from '@angular/core';
import {inject} from 'inversify';
import {userDataType} from '../core/models/user';
import {DataPost} from '../core/models/post';
import {ALLPOSTS} from '../core/models/postList';
import {UserComponent} from '../user/user.component';
import {DateHelper} from '../core/services/DateHelper';
import {PostService} from '../core/services/post.service';
import {UserService} from '../core/services/user.service';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})



export class PostComponent implements OnInit {
  @Input()
  post: DataPost;
  currentUser: userDataType;

  constructor(private userService: UserService) {
    this.currentUser = userService.currentUser;
  }

  showPost() {
    DateHelper.getTimeAgo(this.post.datePublic);
  }

  // editPost() {
    // if (((Guard.isModerator(this.currentUser.group))) ||
    //   (Guard.isOwner(this.currentUser.group, this.currentUser.userInfo.userId, this.value.authorId))) {
    //   // edit post
    //   console.log('editing...');
    // } else {
    //   console.log('Permission denied for editing post');
    // }
  // }

  // deletePost() {
    // if ((Guard.isModerator()) || (Guard.isOwner(this.group, this.CurrentUser.name, this.value.author))) {
    //     // edit post
    // } else {
    //     console.log('Permission denied');
    // }
  // }

  ngOnInit(): void {
    console.log(this.post);
  }
}
