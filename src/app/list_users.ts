import {userDataType} from './core/models/user';

export const AllUsers: userDataType[] = [
  { userId: 1, login: 'igor777',
    password: '12345', lastSeen: new Date(),
    name: 'Igor', surname: 'Dew',
    email: 'igor@gmai.com', phoneNumber: '0969084756',
    birthday: new Date('1935-09-2') },
  { userId: 2, login: 'ray',
    password: '12345', lastSeen: new Date(),
    name: 'Raymond', surname: 'Dew',
    email: 'Ray@gmai.com', phoneNumber: '0969687753',
    birthday: new Date('1965-01-2') },
  { userId: 3, login: 'samuel',
    password: '12345', lastSeen: new Date(),
    name: 'Sam', surname: 'Cat',
    email: 'Sam@gmai.com', phoneNumber: '0969085009',
    birthday: new Date('1967-04-2') }
];
