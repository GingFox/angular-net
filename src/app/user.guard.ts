import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Roles, Rights} from './core/models/user';

export class Guard implements CanActivate {

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): boolean {

  }

  permissions: (string | boolean)[][] = [];

  constructor() {
    const countRights = 14;
    const countRoles = 4;
    this.permissions[0] = [];
    for (let i = 1; i < countRoles; i++) {
      this.permissions[0][i] = Roles[i];
      for (let j = 1; j < countRights; j++) {
        this.permissions[j] = [];
        this.permissions[j][0] = Rights[j - 1];
      }
    }
    this.rightsForUser();
    this.rightsForModerator();
    this.rightsForAdmin();
    console.log(this.permissions);
  }

  static isAdmin(groups: number[]): boolean {
    let result: boolean;
    // console.log(groups);
    result = groups.some(elem => elem === 1);
    return result;
  }

  static isModerator(groups: number[]): boolean {
    let result: boolean;
    // console.log(groups);
    result = groups.some(elem => elem === 2);
    return result;
  }

  static isOwner(groups: number[], userId: number, authorId: number): boolean {
    let result: boolean;

    if (groups.some(elem => elem === 3)) {
      if (userId === authorId) {
        console.log('User is post\'s author');
        result = true;
      } else {
        console.log('User is not post\'s author');
        result = false;
      }
    }

    if (groups.some(elem => elem === 2)) {
      console.log('User is moderator');
      result = true;
    } else {
      console.log('User is not moderator');
      result = false;
    }
    return result;
  }

  rightsForAdmin() {
    for (let i = 1; i <= 7; i++) {
      this.permissions[i][1] = false;
    }
    for (let i = 8; i <= 13; i++) {
      this.permissions[i][1] = true;
    }
  }

  rightsForModerator() {
    for (let i = 1; i <= 2; i++) {
      this.permissions[i][2] = false;
    }
    for (let i = 3; i <= 8; i++) {
      this.permissions[i][2] = true;
    }
    for (let i = 9; i <= 13; i++) {
      this.permissions[i][2] = false;
    }
  }

  rightsForUser() {
    for (let i = 1; i <= 7; i++) {
      this.permissions[i][3] = true;
    }

    for (let i = 8; i <= 13; i++) {
      this.permissions[i][3] = false;
    }
  }
}
