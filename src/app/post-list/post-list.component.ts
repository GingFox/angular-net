import {Component, OnInit} from '@angular/core';
import {DataPost} from '../core/models/post';
import {Observable, of} from 'rxjs';
import {ALLPOSTS} from '../core/models/postList';
import {UserService} from '../core/services/user.service';
import {PostListService} from '../core/services/post-list.service';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  posts$: Observable<DataPost[]>;

  constructor(private postListService: PostListService) {
  }

  getPosts(): void {
    this.posts$ =  this.postListService.getPosts().pipe(map(dataposts => dataposts.filter(post => (post.authorId > 5))));
  }

  ngOnInit(): void {
    this.getPosts();
  }

}

