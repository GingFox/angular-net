
export enum Roles {
  Admin = 1,
  Moderator = 2,
  JustUser = 3,
}

export enum Rights {
  CreatePost,
  CreateComment,
  SendMessage,
  EditPost,
  DeletePost,
  EditComment,
  DeleteComment,
  BannUser,
  EditUser,
  DeleteUser,
  CreateNewUser,
  EditSettingsSystem,
  GetRights
}

export interface UserDataFields {
  userId: number;
  login: string;
  password: string;
  lastSeen: Date;
}

export interface PersonDataFields {
  name: string;
  surname: string;
  email: string;
  phoneNumber: string;
  birthday: Date;
}

export type userDataType = UserDataFields & PersonDataFields;
