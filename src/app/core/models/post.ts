export interface DataPost {
  content: ContentData;
  datePublic: Date;
  imagePost: string;
  authorId: number;
}

interface ContentData {
  header: string;
  text: string;
}

