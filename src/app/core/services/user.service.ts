import {Injectable} from '@angular/core';
import {userDataType} from '../models/user';
import {AllUsers} from '../../list_users';
import {Observable, of} from 'rxjs';
import {MessageService} from './message.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  currentUser: userDataType;

  constructor(private messageService: MessageService) {
  }

  getUsers(): Observable<userDataType[]> {
    this.messageService.add('Userservice: fetched the users');
    return of(AllUsers);
  }

}
