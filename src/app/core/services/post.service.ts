import { Injectable } from '@angular/core';
import {DataPost} from '../models/post';
import {ALLPOSTS} from '../models/postList';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor() { }

  getPosts(): Observable<DataPost[]> {
    return of (ALLPOSTS);
  }
}
