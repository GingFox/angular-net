import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {DataPost} from '../models/post';
import {ALLPOSTS} from '../models/postList';


@Injectable({
  providedIn: 'root'
})
export class PostListService {

  constructor() { }

  getPosts(): Observable<DataPost[]> {
    return of (ALLPOSTS);
  }
}
