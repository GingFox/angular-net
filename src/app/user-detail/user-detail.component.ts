import { Component, OnInit, Input } from '@angular/core';
import {userDataType} from '../core/models/user';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})

export class UserDetailComponent implements OnInit {
  @Input() user: userDataType;
  constructor() { }

  ngOnInit(): void {
  }

}
