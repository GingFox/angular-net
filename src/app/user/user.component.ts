import {Component, OnInit} from '@angular/core';
// import {AllUsers} from '../list_users';
import {UserService} from '../core/services/user.service';
import {userDataType} from '../core/models/user';
import { MessageService } from '../core/services/message.service';
import {injectable} from 'inversify';
import 'reflect-metadata';
import {from} from 'rxjs';
import {DataPost} from '../core/models/post';
import {Roles, Rights} from '../core/models/user';
import {DateHelper} from '../core/services/DateHelper';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

@injectable()
export class UserComponent implements OnInit {
  users: userDataType[];
  selectedUser: userDataType;
  group: number[];

  constructor(private userService: UserService, private messageService: MessageService) {
    this.group = [];
    this.group.push(Roles.JustUser);
    this.group.push(Roles.Admin);
    // this.getRights('moderator');
  }

  onSelect(user: userDataType): void {
    this.selectedUser = user;
    this.messageService.add(`Selected user id=${user.userId}: ${user.login}`);
  }

  getUsers(): void {
    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }


  // getRights(access: string) {
  //   if (Guard.isAdmin(this.group)) {
  //     if (access === 'moderator') {
  //       this.group.push(Roles.Moderator);
  //       // console.log('Role was added');
  //     } else if (access === 'admin') {
  //       this.group.push(Roles.Admin);
  //     } else {
  //       console.log('Error');
  //     }
  //   } else {
  //     console.log('Permission denied');
  //   }
  // }
  //
  // bannUser() {
  //   if (Guard.isAdmin(this.group)) {
  //     // bann user
  //   } else {
  //     console.log('Permission denied');
  //   }
  // }
  //
  // editUser() {
  //   if (Guard.isAdmin(this.group)) {
  //     // edit user
  //   } else {
  //     console.log('Permission denied');
  //   }
  // }
  //
  // deleteUser() {
  //   if (Guard.isAdmin(this.group)) {
  //     // delete user
  //   } else {
  //     console.log('Permission denied');
  //   }
  // }
  //
  // createNewUser() {
  //   if (Guard.isAdmin(this.group)) {
  //     // create new user
  //   } else {
  //     console.log('Permission denied');
  //   }
  // }
  //
  // editSettingsSystem() {
  //   if (Guard.isAdmin(this.group)) {
  //     // edit
  //   } else {
  //     console.log('Permission denied');
  //   }
  // }

  showInfo() {
    console.log(this.selectedUser.name, this.selectedUser.surname);
    console.log(this.selectedUser.birthday.getDate());
      // DateHelper.nameMonth(this.selectedUser.birthday.getMonth()),
      // this.selectedUser.birthday.getFullYear());
    // console.log('Age: ', this.getAge());
  }

  // createPost() {
  //   if (!Guard.isAdmin(this.group) && !Guard.isModerator(this.group)) {
  //     // create post
  //   } else {
  //     console.log('Permission denied');
  //   }
  // }
  showPosts() {
  }

  showComments() {
  }

  signOut() {
  }

  sendMessage() {
  }
  //
  // createPost() {
  //   if (!Guard.isAdmin(this.group) && !Guard.isModerator(this.group)) {
  //     // create post
  //   } else {
  //     console.log('Permission denied');
  //   }
  // }

  createComment() {
  }

  getAge(): number {
    const ageMs = Date.now() - this.selectedUser.birthday.getTime();
    const ageDate = new Date(ageMs); // miliseconds from epoch
    return Math.abs(ageDate.getFullYear() - 1970);
  }

  ngOnInit(): void {
    this.getUsers();
  }

}
